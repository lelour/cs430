package Project;


import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SearchingInputDialog extends JDialog {
	
	final JTextField jtf1;  // modify two input 
	final JLabel jlinfo;
	public String str1 = "";
	public int pass = 0;  // 0-verify not pass, 1-verify pass
	
	public SearchingInputDialog() {
		setTitle("Searching Arrays Input");
		setModal(true);
		setSize(400,200);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);  // destroy dialog after close
		setLocationRelativeTo(null);
		JLabel jl1 = new JLabel("Integers : ");
		jtf1 = new JTextField(500);
		
		JPanel jp = new JPanel(new GridLayout(3,2));
		jp.add(jl1);
		jp.add(jtf1);
		
		JButton jb = new JButton("Confirm");
		
		jlinfo = new JLabel("INFO : ", JLabel.CENTER);
		jlinfo.setText("input positive integers in textfield, separated by comma");
		
		jb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				str1 = jtf1.getText();
				
				if (str1.equals("")) {
					pass = 0;
					JOptionPane.showMessageDialog(null, "You still have no input!", "WARNING", JOptionPane.WARNING_MESSAGE);
				} else {
					Pattern pattern = Pattern.compile("\\d+(,\\d+)*");  // digit and comma only
					if (!pattern.matcher(str1).matches()) {
						pass = 0;
						JOptionPane.showMessageDialog(null, "Data Format Error!", "WARNING", JOptionPane.WARNING_MESSAGE);
					} else {
						pass = 1;
						jlinfo.setText("Input VERIFIED, click CLOSE to return!");
					}
				}
				
			}
		});
		
		add(jp);
		add(jlinfo, BorderLayout.NORTH);
		add(jb, BorderLayout.SOUTH);
	}
	
}
