package Project;

public class MedianFinding {
	
	public String progress = "";
	
    public double findMedianSortedArrays(ArrayCollection<Integer> nums1, int m, ArrayCollection<Integer> nums2, int n) {
      
    	if (m > n) {
    		progress = " Length of Array1 is " + m + " longer than length of Array2 = " + n + " . Swap these two arrays first.\r\n  ";
            return findMedianSortedArrays(nums2, n, nums1, m);
        }
    	// m=0 and n=0 is not allowed
        if (m == 0) {
        	progress += " Because length of Array1 is 0, so just get the median of Array2. \r\n";
        	return n % 2 == 0 ? (nums2.getValue(n/2) + nums2.getValue((n-1)/2)) / 2.0 : nums2.getValue(n/2);
        }

        int left = 0, right = m;
        // median1：maximum value of left part
        // median2：minimum value of right part
        int median1 = 0, median2 = 0;

        progress += "Length of Array1 m = " + m + " , Length of Array2 n = " + n + " \r\n\r\n  ";
        progress += "The progress is :\r\n  ";
        progress += "On startup:\r\n  ";
        if (m==1) {
        	progress += "Array1 = [" + nums1.getValue(0) + "[0]]   Maximum Value Of Left Part = " + median1 + " \r\n  ";
        } else {
        	progress += "Array1 = [" + nums1.getValue(0) + "[0], ... , " + nums1.getValue(m-1) + "[m-1]]   Maximum Value Of Left Part = " + median1 + " \r\n  ";
        } 
        progress += "Array2 = [" + nums2.getValue(0) + "[0], ... , " + nums2.getValue(n-1) + "[n-1]]   Miminum Value Of Right Part = " + median2 + " \r\n  ";
        progress += "Left pointer = " + left + " , Right pointer = " + right + "\r\n  ";
        progress += "\r\n  ";
        
        while (left <= right) {
            // Left part contains nums1[0 .. i-1] and nums2[0 .. j-1]
            // Right part contains nums1[i .. m-1] and nums2[j .. n-1]
            int i = (left + right) / 2;
            int j = (m + n + 1) / 2 - i;
            
            progress += "i = (LefPointert + RightPointer )/2 and change j correspondingly.\r\n  ";
            progress += "i = " + i + " , j = " + j + "\r\n  ";
            if (m==1) {
            	progress += "Left Part of Array1 = [" + nums1.getValue(0) + "[0]]     ";
            	progress += "Right Part of Array1 = [" + nums1.getValue(0) + "[0]]    \r\n  ";
            } else {
                progress += "Left Part of Array1 = [" + nums1.getValue(0) + "[0], ... , " + nums1.getValue(i-1) + "[i-1]]     ";
                progress += "Right Part of Array1 = [" + nums1.getValue(i) + "[i], ... , " + nums1.getValue(m-1) + "[m-1]]    \r\n  ";
            }
            if (j==0) {
            	progress += "Left Part of Array2 = [" + nums2.getValue(0) + "[0]]     ";
                progress += "Right Part of Array2 = [" + nums2.getValue(j) + "[0], ... , " + nums2.getValue(n-1) + "[n-1]]    \r\n  ";
            } else {
            	progress += "Left Part of Array2 = [" + nums2.getValue(0) + "[0], ... , " + nums2.getValue(j-1) + "[j-1]]     ";
                progress += "Right Part of Array2 = [" + nums2.getValue(j) + "[j], ... , " + nums2.getValue(n-1) + "[n-1]]    \r\n  ";
            }
            
            
            // nums_im1, nums_i, nums_jm1, nums_j stands for nums1[i-1], nums1[i], nums2[j-1], nums2[j]
            int nums_im1 = (i == 0 ? Integer.MIN_VALUE : nums1.getValue(i-1));
            int nums_i = (i == m ? Integer.MAX_VALUE : nums1.getValue(i));
            int nums_jm1 = (j == 0 ? Integer.MIN_VALUE : nums2.getValue(j-1));
            int nums_j = (j == n ? Integer.MAX_VALUE : nums2.getValue(j));

            if (nums_im1 <= nums_j) {
                median1 = Math.max(nums_im1, nums_jm1);
                median2 = Math.min(nums_i, nums_j);
                
                progress += "Update Boundry Value when Maximum Value Of Left Part of Array1 is not greater than Minimum Value of Right Part of Array2. \r\n  ";
                
                left = i + 1;
            } else {
                right = i - 1;
            }
            
            progress += "Maximum Value Of Left Part = " + median1 + "  Mimimum Value Of Right Part = " + median2 + " \r\n  ";
            progress += "Left pointer = " + left + " , Right pointer = " + right + "\r\n  ";
            
            progress += "\r\n  ";
        }

        progress += "Left Pointer > Right Pointer, then exit loop.\r\n  ";
        if ((m+n)%2==0) {
        	progress += " For m+n is even, so result is (Maximun Value of Left Part + Minimum Value of Right Part)/2. \r\n  ";
        } else {
        	progress += "For m+n is odd, so result is Maximum Value of Left Part. \r\n  ";
        }
        
        progress += "\r\n";
        
        return (m + n) % 2 == 0 ? (median1 + median2) / 2.0 : median1;
    }
}
