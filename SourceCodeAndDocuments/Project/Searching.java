package Project;

public class Searching {
	
	public String progress = "";
	
	public String int2Roman(int intnum) {
		String[] romans = { "x", "v", "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };
		int[] ints = { 10000, 5000, 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };
		
		progress = "Integer = " + intnum + " = ";
		
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < ints.length; i++) {
			while (intnum >= ints[i]) {
				sb.append(romans[i]);
				intnum -= ints[i];
				progress += ints[i] + "(" + romans[i] + ")+";
			}
		}
		
		progress = progress.substring(0, progress.length()-1);
		
		return sb.toString();
	}

}
