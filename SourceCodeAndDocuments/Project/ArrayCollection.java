package Project;

import java.lang.reflect.Array;

public class ArrayCollection<T extends Comparable<T>> {
	// default setting
	protected T[] elements;  // array to hold list's elements
	protected int numElements = 0;  // number of elements
	protected int timeComplexity =0; // time complexity counter when found
	
	//set by find method
	protected boolean found;  //true if target found, otherwise false
	protected int location;   // indicates location of target if found

	// constructor
	@SuppressWarnings("unchecked")
	public ArrayCollection(T[] a) {
		elements = (T[]) Array.newInstance(a.getClass().getComponentType(), a.length);
		for (int i=0; i<a.length; i++) elements[i] = a[i];
		numElements = a.length;
	}

//	// old code with errors
//	@SuppressWarnings("unchecked")
//	public ArrayCollection(int capacity) {
//		elements = (T[]) new Object[capacity];
//	}
	
	
	// sequential search
	protected void find(T target) {
		location = 0;  found = false;
		while (location < numElements) {
			if (elements[location].equals(target)) {
				found = true;
				timeComplexity = location + 1;
				return;
			} else {
				location++;
			}
		}
	}
	
	public boolean remove(T target) {
		find(target);
		if (found) {
			elements[location] = elements[numElements -1];
			elements[numElements -1] = null;
			numElements--;
		}
		return found;
	}
	
	public boolean contains(T target) {
		find(target);
		return found;
	}
	
	public T get(T target) {
		find(target);
		if (found) {
			return elements[location];
		} else {
			return null;
		}
	}
	
	public T getValue(int location) {
		return elements[location];
	}
	
	public boolean isFull() {
		return (numElements == elements.length);
	}
	
	public boolean isEmpty() {
		return (numElements == 0);
	}
	
	public int size() {
		return numElements;
	}
	
	// ascending
	public void bubbleSort() {
		for (int i=0; i < elements.length-1; i++) {
			for (int j=0; j < elements.length-1-i; j++) {
				if (elements[j].compareTo(elements[j+1]) > 0) {
					T temp = elements[j];
					elements[j] = elements[j+1];
					elements[j+1] = temp;
				}
			}
		}
	}
	
	public void QuickSort(){
		Quick_Sort(0,elements.length-1);
	}
	
	public void Quick_Sort(int left ,int right){
		if (left>=right){
			return ;
		}
		int k = Partition(left,right);
		Quick_Sort(left,k-1);
		Quick_Sort(k+1,right);
		return;
	}
   
	public int Partition(int left ,int right){

		int i=left;
		int j=i;
		for (; i <right ; i++) {
			if (elements[i].compareTo(elements[right]) < 0){
				T temp=elements[i];
				elements[i]=elements[j];
				elements[j]=temp;
				j++;	
			}
		}
		T temp=elements[right];
		elements[right]=elements[j];
		elements[j]=temp;
		return j;
	}

	// binary search
	public void binarySearch(T target) {
		int low = 0;
		int high = numElements -1;
		timeComplexity = 0;
		while (low <= high) {
			timeComplexity += 1;
			int middle = low + (high-low)/2;
			int cmp = target.compareTo(elements[middle]);
			if (cmp<0) high = middle - 1;
			else if (cmp>0) low = middle + 1;
			else {
				location = middle;
				return;
			}
		}
	}

	// for testing
	public String toString() {
		String result = "";
		for (int i=0; i< elements.length; i++) {
			result += elements[i] + " ";
		}
		return result;
	}
	
	public String toString(int length, String sep) {
		String result = "";
		for (int i=0; i< length-1; i++) {
			result += elements[i] + sep;
		}
		result += elements[length-1];
		return result;
	}

}
