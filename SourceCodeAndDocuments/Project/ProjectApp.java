package Project;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Pattern;

import javax.swing.*;
import javax.swing.border.EtchedBorder;

public class ProjectApp {

	static JTextArea textArea;
	static JLabel hintbar;
	static JCheckBoxMenuItem mf;  // Median Finding
	static JCheckBoxMenuItem ss;  // Searching
	
	static String oh = "Operation Hint: ";
	static String stringDA[] = new String[100];
	static int stringCount; // counter of string data amount
	static Integer randomIntegers1[] = new Integer[2000];  // array1 length is 2000 at most
	static Integer randomIntegers2[] = new Integer[2000];  // array2 length is 2000 at most
	static Integer m = 0;  // array length of array1
	static Integer n = 0;  // array length of array2
	static ArrayCollection<Integer> sortedArray1;
	static ArrayCollection<Integer> sortedArray2;
	
	static boolean mfDataExist; // median finding data imported or generated
	static boolean dsDataExist; // searching data imported or generated
	static int dataType; // 1-data from file, 2-generated random integers, 3-inputed data
	static int fileError = 0;   // 0 - no error, else - error

	static boolean notFound; // not found - true
	static String finalText; // final show on textArea
	
	public static void main(String[] args) {

		mfDataExist = false;
		dsDataExist = false;
		
		// Create Top Window Frame
		JFrame jf = new JFrame("CS430 Project Application for Median Finding and Searching");
		jf.setSize(850,500);  // Window Size
		jf.setLocationRelativeTo(null);  // set window to central
		jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		// set menu
		JMenuBar jb = new JMenuBar();
		jb.add(createFileMenu());
		jb.add(createMedianFindingMenu());
		jb.add(createSearchingMenu());
		jb.add(createHelpMenu());
		jf.setJMenuBar(jb);
		
		// set Panel Container
		JPanel panel1 = new JPanel();		
		JPanel panel2 = new JPanel();
		
		// add text area
		textArea = new JTextArea(24,68);
		textArea.setLineWrap(true);
		textArea.setAutoscrolls(true);
		textArea.setEditable(false);
		panel1.add(new JScrollPane(textArea));
		
		
		// add hint bar
		hintbar = new JLabel(oh);
		hintbar.setForeground(Color.RED);
		hintbar.setHorizontalAlignment(JLabel.LEFT);
		hintbar.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
		
		// add panel to window
		//jf.setContentPane(panel1);	
		jf.add(panel1, BorderLayout.NORTH);
		jf.add(panel2, BorderLayout.CENTER);
		//add hint bar
		jf.add(hintbar, BorderLayout.SOUTH);
		
		
		// show the window
		jf.setVisible(true);
		
		// Operation Start Here
		hintbar.setText(oh + "Select MedianFinding or Searching, import data from file or generate random data or input data, then do task.");
		
	}
	
	// modify the FIle menu
	private static JMenu createFileMenu() {
		JMenu fileMenu = new JMenu("File(F)");
		fileMenu.setMnemonic(KeyEvent.VK_F);
				
		JMenuItem item = new JMenuItem("Exit(E)", KeyEvent.VK_E);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
		item.setToolTipText("Exit");
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				System.exit(0);
			}
		});
		fileMenu.add(item);
		
		return fileMenu;
	}

	// Algorithm Menu
	private static JMenu createMedianFindingMenu() {
		JMenu mfMenu = new JMenu("MedianFinding(M)");
		mfMenu.setMnemonic(KeyEvent.VK_M);
				
		JMenuItem item = new JMenuItem("Import MF Data From File(I)", KeyEvent.VK_I);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, ActionEvent.CTRL_MASK));
		item.setToolTipText("Import the integer data from file.");
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				JFileChooser chooser = new JFileChooser();
				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					File file = chooser.getSelectedFile();
					readMFDataFile(file);
					if (mfDataExist) {
						hintbar.setText(oh+"Data file imported. Now do median finding, then result will output.");
					} else {
						JOptionPane.showMessageDialog(null, "Data file could not be empty or file format error!", "WARNING", JOptionPane.WARNING_MESSAGE);
					}
					
				}
			}
		});
		mfMenu.add(item);
		
		item = new JMenuItem("Generate Random MF Data(G)", KeyEvent.VK_G);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK));
		item.setToolTipText("Generate 100 random integer numbers for array1 and 100 random integer numbers for array2.");
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				generateRandomMFIntegers();
				if (mfDataExist) {
					hintbar.setText(oh+"200 random integers generated. Now do median finding, then result will output.");
				} else {
					JOptionPane.showMessageDialog(null, "Data generation meets error!", "WARNING", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		mfMenu.add(item);
		
		item = new JMenuItem("Input MF Data(T)", KeyEvent.VK_T);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.CTRL_MASK));
		item.setToolTipText("Input 100 integer numbers for array1 and another 100 integer numbers for array2, separated by comma.");
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				inputMFArrays();
				if (mfDataExist) {
					hintbar.setText(oh+"integers inputed. Now do median finding, then result will output.");
				} else {
					JOptionPane.showMessageDialog(null, "Data has not inputed yet!", "WARNING", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		mfMenu.add(item);
		
		mfMenu.addSeparator();
		
		item = new JMenuItem("Median Finding(D)", KeyEvent.VK_D);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, ActionEvent.CTRL_MASK));
		item.setToolTipText("Return the median of the two sorted arrays.");
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if (mfDataExist) {
					getMedianResult();
					hintbar.setText(oh+"The result is outputed.");
				} else {
					JOptionPane.showMessageDialog(null, "Please choose one way to load data first for median finding!", "WARNING", JOptionPane.WARNING_MESSAGE);
				}
				
			}
		});
		mfMenu.add(item);
		
		return mfMenu;		
	}

	// DataStructure Menu
	private static JMenu createSearchingMenu() {
		JMenu dsMenu = new JMenu("Searching(S)");
		dsMenu.setMnemonic(KeyEvent.VK_S);
				
		JMenuItem item = new JMenuItem("Import Searching Data From File(R)", KeyEvent.VK_R);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		item.setToolTipText("Import the integer data from file.");
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				JFileChooser chooser = new JFileChooser();
				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					File file = chooser.getSelectedFile();
					readDSDataFile(file);
					if (dsDataExist) {
						hintbar.setText(oh+"Data file imported. Now do Searching, then result will output.");
					} else {
						JOptionPane.showMessageDialog(null, "Data file could not be empty or file format error!", "WARNING", JOptionPane.WARNING_MESSAGE);
					}
					
				}
			}
		});
		dsMenu.add(item);
		
		item = new JMenuItem("Generate Random Searching Data(N)", KeyEvent.VK_N);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		item.setToolTipText("Generate 20 random integer numbers for translation to roman numerals.");
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				generateSearchingRandomIntegers();
				hintbar.setText(oh+"20 random integers generated. Now do Searching, then result will output.");
				
			}
		});
		dsMenu.add(item);
		
		item = new JMenuItem("Input Searching Data(U)", KeyEvent.VK_U);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.CTRL_MASK));
		item.setToolTipText("Input integer numbers for translation to roman numerals.");
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				inputSearchingIntegers();
				if (dsDataExist) {
					hintbar.setText(oh+"integers inputed. Now do Searching, then result will output.");
				} else {
					JOptionPane.showMessageDialog(null, "Data has not inputed yet!", "WARNING", JOptionPane.WARNING_MESSAGE);
				}
				
				
			}
		});
		dsMenu.add(item);
		
		dsMenu.addSeparator();
		
		item = new JMenuItem("Searching(C)", KeyEvent.VK_C);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK));
		item.setToolTipText("Input integer numbers for array1 and array2, separated by comma.");
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if (dsDataExist) {
					getSearchingResult();
					hintbar.setText(oh+"The result is outputed.");
				} else {
					JOptionPane.showMessageDialog(null, "Please choose one way to load data first for searching!", "WARNING", JOptionPane.WARNING_MESSAGE);
				}
				
			}
		});
		dsMenu.add(item);
		
		return dsMenu;
		
	}
	
	// modify Help Menu
	private static JMenu createHelpMenu() {
		JMenu helpMenu = new JMenu("Help(H)");
		helpMenu.setMnemonic(KeyEvent.VK_H);
		
		JMenuItem item = new JMenuItem("Help(P)", KeyEvent.VK_P);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, ActionEvent.CTRL_MASK));
		item.setToolTipText("Help on operation instructions.");
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				JFrame helpFrame = new JFrame("Help");
				helpFrame.setBounds(200,200,600,600);
				helpFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
				
				JEditorPane jhelp = new JEditorPane();
				jhelp.setEditable(false);
				
				try {
					jhelp.setPage(getClass().getResource("help.html"));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				JScrollPane scrollPane = new JScrollPane(jhelp);
				helpFrame.add(scrollPane);
				
				helpFrame.setVisible(true);
				
			}
		});
		helpMenu.add(item);
		
		helpMenu.addSeparator();
		
		item = new JMenuItem("Copyright(O)", KeyEvent.VK_O);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		item.setToolTipText("Copyright by Jian Li.");
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				JOptionPane.showMessageDialog(null, "Copyright by Jian Li", "Copyright", JOptionPane.PLAIN_MESSAGE);
			}
		});
		helpMenu.add(item);
		
		return helpMenu;
		
	}

	// import data from file
	// two lines, first is for array1, second is for array2, all separated by comma
	public static void readMFDataFile(File file) {
		BufferedReader bReader;
		try {
			fileError = 0;  // init to normal
			bReader = new BufferedReader(new FileReader(file));
			StringBuffer sBuffer = new StringBuffer();
			int i = 0;
			String str;
			while ((str = bReader.readLine())!=null) {
				stringDA[i] = str;
				i += 1;
			}
			if (i != 0) {
				// amount of string data
				// stringCount = i;
				// convert to array
				Pattern pattern = Pattern.compile("^-?\\d+(,-?\\d+)*");  // digit and comma only
				//Pattern pattern = Pattern.compile("^[-,0-9]+$");  // digit and comma only
				
				// array1
				if ((i == 1) && (stringDA[0].length() <= 0)) {
					fileError = 1; // str should not be empty when only one string
				} else if (stringDA[0].length() == 0) {
					m = 0;
				} else if (!pattern.matcher(stringDA[0]).matches()) {
					fileError = 1;  // error format
				} else {
					String[] arr1 = stringDA[0].split(",");
					m = arr1.length;
					for (int ai=0; ai < m; ai++) {
						randomIntegers1[ai] = Integer.valueOf(arr1[ai]);
					}
					// create sorted array list
					sortedArray1 = new ArrayCollection<Integer>(randomIntegers1);
					sortedArray1.Quick_Sort(0, m-1);
				}
				
				// array2
				if (i == 1) {
					n = 0;
				} else if ((i==2) && (m==0) && (stringDA[1].length() == 0)) {
					fileError = 1; // str should not be empty when both are empty
				} else if ((i==2) && (m!=0) && (stringDA[1].length() == 0)) {
					n = 0;
				} else if (!pattern.matcher(stringDA[1]).matches()) {
					fileError = 1;  // error format
				} else {
					String[] arr2 = stringDA[1].split(",");
					n = arr2.length;
					for (int ai=0; ai < n; ai++) {
						randomIntegers2[ai] = Integer.valueOf(arr2[ai]);
					}
					// create sorted array list
					sortedArray2 = new ArrayCollection<Integer>(randomIntegers2);
					sortedArray2.Quick_Sort(0, n-1);
				}
				
				// show in panel
				if (fileError == 0) {
					// construct sBuffer
					String ss = "Integer Arrays loaded from file for Median Finding:\r\narray1:\r\n";
					if (m != 0)
						ss += sortedArray1.toString(m, ",");
					ss += "\r\narray2:\r\n";
					if (n != 0)
						ss += sortedArray2.toString(n, ",");
					
					textArea.setText(ss);
					mfDataExist = true;
					dsDataExist = false;  //mutual
					dataType = 1;
				}				
			} else {
				mfDataExist = false;
			}	
		} catch (Exception e) {
			// handle exception
			e.printStackTrace();
		}
	}
	
	// generate 40 non-repeat random integers for median finding
	public static void generateRandomMFIntegers() {
		// generate 40 non-repeat random integer
		// array1
		int rand = 0;
		for (int i=0; i<100; i++) {
			while(true) {
				rand = (int)(3000*Math.random());
				for (int j=0; j<i; j++) {
					if (randomIntegers1[j]==rand) {
						rand = -1;
						break;
					}
				}
				if (rand != -1) {
					randomIntegers1[i] = rand;
					break;
				}
			}
		}
		m = 100;
		
		// array2
		for (int i=0; i<100; i++) {
			while(true) {
				rand = (int)(3000*Math.random());
				for (int j=0; j<i; j++) {
					if (randomIntegers2[j]==rand) {
						rand = -1;
						break;
					}
				}
				if (rand != -1) {
					randomIntegers2[i] = rand;
					break;
				}
			}
		}
		n = 100;
		
		// create sorted array list
		sortedArray1 = new ArrayCollection<Integer>(randomIntegers1);
		sortedArray1.Quick_Sort(0, 99);
		
		// create sorted array list
		sortedArray2 = new ArrayCollection<Integer>(randomIntegers2);
		sortedArray2.Quick_Sort(0, 99);
		
		String ss = "Random Integer Array generated for Median Finding:\r\narray1:\r\n";
		ss += sortedArray1.toString(100, ",");
		ss += "\r\narray2:\r\n";
		ss += sortedArray2.toString(100, ",");
		
		textArea.setText(ss);
		mfDataExist = true;
		dsDataExist = false;  //mutual exclusion
		dataType = 2;
	}

	public static void inputMFArrays() {
		MFInputDialog mfdialog = new MFInputDialog();
		mfdialog.setVisible(true);
		
		if (mfdialog.pass == 1) {
			
			if (mfdialog.str1.length() <= 0) {
				m = 0;
			} else {
				String[] arr1 = mfdialog.str1.split(",");
				m = arr1.length;
				for (int ai=0; ai < m; ai++) {
					randomIntegers1[ai] = Integer.valueOf(arr1[ai]);
				}
				// create sorted array list
				sortedArray1 = new ArrayCollection<Integer>(randomIntegers1);
				sortedArray1.Quick_Sort(0, m-1);
			}

			
			if (mfdialog.str2.length() <= 0) {
				n = 0;
			} else {
				String[] arr2 = mfdialog.str2.split(",");
				n = arr2.length;
				for (int ai=0; ai < n; ai++) {
					randomIntegers2[ai] = Integer.valueOf(arr2[ai]);
				}
				// create sorted array list
				sortedArray2 = new ArrayCollection<Integer>(randomIntegers2);
				sortedArray2.Quick_Sort(0, n-1);
			}
			
			
			// show
			String ss = "Integer Arrays Inputed for Median Finding:\r\narray1:\r\n";
			if (m !=0)
				ss += sortedArray1.toString(m, ",");
			ss += "\r\narray2:\r\n";
			if (n != 0)
				ss += sortedArray2.toString(n, ",");
			
			textArea.setText(ss);
			mfDataExist = true;
			dsDataExist = false;  //mutual
			dataType = 3;
		}

	}
	
	public static void getMedianResult() {
		
		MedianFinding resultMF = new MedianFinding();
		double result = resultMF.findMedianSortedArrays(sortedArray1, m, sortedArray2, n);
		
		String ss = "Median Finding of two sorted array is : ";
		ss += result;
		
		ss += "  \r\n\r\n  " + resultMF.progress;
		
		textArea.setText(ss);
		
		
	}

	// import data from file
	// one lines, integers less than 10000, all separated by comma
	public static void readDSDataFile(File file) {
		BufferedReader bReader;
		try {
			fileError = 0;  // init to normal
			bReader = new BufferedReader(new FileReader(file));
			StringBuffer sBuffer = new StringBuffer();
			int i = 0;
			String str;
			while ((str = bReader.readLine())!=null) {
				stringDA[i] = str;
				i += 1;
			}
			if (i != 0) {
				// amount of string data
				// stringCount = i;
				// convert to array
				Pattern pattern = Pattern.compile("\\d+(,\\d+)*");  // digit and comma only
				//Pattern pattern = Pattern.compile("^[-,0-9]+$");  // digit and comma only
				
				// array1
				if (!pattern.matcher(stringDA[0]).matches()) {
					fileError = 1;  // error format
				} else {
					String[] arr1 = stringDA[0].split(",");
					m = arr1.length;
					for (int ai=0; ai < m; ai++) {
						randomIntegers1[ai] = Integer.valueOf(arr1[ai]);
					}
				}
				
				// show in panel
				if (fileError == 0) {
					// construct sBuffer
					String ss = "Integers loaded from file for Searching:\r\n";
					ss += stringDA[0];
					
					textArea.setText(ss);
					mfDataExist = false;
					dsDataExist = true;  //mutual
					dataType = 1;
				}				
			} else {
				dsDataExist = false;
			}	
		} catch (Exception e) {
			// handle exception
			e.printStackTrace();
		}
	}

	// generate 20 non-repeat random integers less than 10000 for searching
	public static void generateSearchingRandomIntegers() {
		// generate 20 non-repeat random integer
		// array1
		int rand = 0;
		for (int i=0; i<20; i++) {
			while(true) {
				rand = (int)(10000*Math.random());
				for (int j=0; j<i; j++) {
					if (randomIntegers1[j]==rand) {
						rand = -1;
						break;
					}
				}
				if (rand != -1) {
					randomIntegers1[i] = rand;
					break;
				}
			}
		}
		m = 20;
		
		String ss = "Random Integer Array generated for Searching:\r\n";
		for (int i=0; i<20-1; i++) {
			ss += randomIntegers1[i].toString() + ",";
		}
		ss += randomIntegers1[20-1].toString();
		textArea.setText(ss);
		mfDataExist = false;
		dsDataExist = true;  //mutual exclusion
		dataType = 2;
	}

	public static void inputSearchingIntegers() {
		SearchingInputDialog dsdialog = new SearchingInputDialog();
		dsdialog.setVisible(true);
		
		if (dsdialog.pass == 1) {
			
			String[] arr1 = dsdialog.str1.split(",");
			m = arr1.length;
			for (int ai=0; ai < m; ai++) {
				randomIntegers1[ai] = Integer.valueOf(arr1[ai]);
			}
			
			// show
			String ss = "Integer Arrays Inputed for Searching:\r\n";
			ss += dsdialog.str1;
			
			textArea.setText(ss);
			mfDataExist = false;
			dsDataExist = true;  //mutual
			dataType = 3;
		}

	}
	
	public static void getSearchingResult() {
		
		Searching resultDS = new Searching();
		String ss = "Searching of integer array is : \r\n";
		String result;
		
		for (int i = 0; i<m; i++) {
			result = resultDS.int2Roman(randomIntegers1[i]);
			ss += randomIntegers1[i].toString() + " = " + result + "\r\n  The procedure is : " + resultDS.progress + "\r\n";
		}
		
		textArea.setText(ss);		
		
	}
}
